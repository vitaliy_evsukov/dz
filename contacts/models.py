## -*- coding: utf-8 -*-
from django.db import models

class Contact(models.Model):
    name = models.CharField(max_length=150, verbose_name='Имя')
    family = models.CharField(max_length=35, verbose_name='Фамилия')
    phone_number = models.CharField(max_length=20, blank=True, verbose_name='Номер Телефона')
    email = models.EmailField(max_length=30, blank=True, verbose_name='Почта')
    address = models.CharField(max_length=150, blank=True, verbose_name='Адресс')
    def __unicode__(self):
        return self.name
