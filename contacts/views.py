# -*- coding: utf-8 -*-
from django.shortcuts import render
from .models import Contact
from django.http import HttpResponseRedirect

def contacts(request):
    contact_list = Contact.objects.all()
    context = {'contacts':contact_list}
    return render(request, 'contacts/contacts.html', context)


def delete(request):
    cont_id = request.POST['id']
    cont = Contact.objects.get(id = cont_id)
    cont.delete()
    return HttpResponseRedirect("/contacts")
    
    

def add(request):
    post_name = request.POST['name']
    post_family = request.POST['family']
    post_phone = request.POST['phone_number']
    post_email = request.POST['email']
    post_address = request.POST['address']
    New = Contact(name = post_name, family = post_family, phone_number = post_phone, email = post_email, address = post_address)
    New.save()
    return HttpResponseRedirect("/contacts")

def edit(request):
    if request.POST['edited'] == "6":
        post_id = request.POST['id']
        post_name = request.POST['name']
        post_family = request.POST['family']
        post_phone = request.POST['phone_number']
        post_email = request.POST['email']
        post_address = request.POST['address']
        New = Contact(id = post_id, name = post_name, family = post_family, phone_number = post_phone, email = post_email, address = post_address)
        New.save()
        return HttpResponseRedirect("/contacts")
    else:
        post_id = request.POST['id']
        context = {'editable' : Contact.objects.get(id = post_id)}
        return render(request, "contacts/contact_edit.html", context)