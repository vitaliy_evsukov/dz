# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('contacts', '0003_auto_20150506_2024'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact',
            name='family',
            field=models.CharField(default=datetime.datetime(2015, 5, 31, 19, 40, 18, 45000, tzinfo=utc), max_length=35),
            preserve_default=False,
        ),
    ]
