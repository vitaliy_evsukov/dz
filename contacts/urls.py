from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.contacts, name='contacts'),
    url(r'delete', views.delete, name='delete'),
    url(r'add', views.add, name='add'),
    url(r'edit', views.edit, name='edit'),
]